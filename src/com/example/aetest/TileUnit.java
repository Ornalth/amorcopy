package com.example.aetest;

import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class TileUnit extends Sprite
{

	ClickListener listener;

	public interface ClickListener
	{
		void clicked(Sprite s);
	}

	public TileUnit(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
	}

	public void setListener(ClickListener l)
	{
		listener = l;
	}

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			float pTouchAreaLocalX, float pTouchAreaLocalY)
	{

		this.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
				pSceneTouchEvent.getY() - this.getHeight() / 2);
		if (pSceneTouchEvent.isActionUp())
		{
			listener.clicked(this);
		}
		return true;
	}

}
