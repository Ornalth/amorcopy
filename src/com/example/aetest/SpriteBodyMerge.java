package com.example.aetest;

import org.andengine.entity.sprite.Sprite;

import com.badlogic.gdx.physics.box2d.Body;

public interface SpriteBodyMerge
{
	Body getBody();
	Sprite getSprite();
}
