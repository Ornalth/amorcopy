package com.example.aetest.Enemy;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.example.aetest.Player.Player;

public class MafiaBlob extends Enemy
{

	float elapsedTotal = 0;
	private static final float elapsedRequired = 1.5f;

	public MafiaBlob(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
	}

	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		elapsedTotal += elapsedTime;
		if (elapsedTotal > elapsedRequired)
		{
			shootBullet(p);
		}
		else
		{
			Vector2 speed = body.getLinearVelocity();
			float speedX = speed.x;
			float speedY = speed.y;
			if (speed.x < velocityX)
			{
				speedX += 2;
			}
			else if (speed.x > velocityX)
			{
				speedX -= 2;
			}

			if (speed.y < velocityY)
			{
				speedY += 2;
			}
			else if (speed.y > velocityY)
			{
				speedY -= 2;
			}

			body.setLinearVelocity(speedX, speedY);
		}
		super.makeMove(p, elapsedTime);
	}

	@Override
	public int getScore()
	{
		return 25;
	}

	private void shootBullet(Player player)
	{
		if (!isDead())
		{
			if (listener.shootBullet(this))
			{
				elapsedTotal = 0;
				body.setLinearVelocity(0, 0);
			}
			else
			{
				elapsedTotal /= 2;
			}
		}
	}

}
