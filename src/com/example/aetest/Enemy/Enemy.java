package com.example.aetest.Enemy;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.physics.box2d.Body;
import com.example.aetest.SpriteBodyMerge;
import com.example.aetest.Player.Player;

public abstract class Enemy implements SpriteBodyMerge
{
	int health = 10;
	protected float velocityX;
	protected float velocityY;
	// int score = 5;
	boolean hit = false;
	protected EnemyListener listener;
	public Body body;
	public Sprite sprite;

	public interface EnemyListener
	{
		void enemyMoved(Enemy enemy);

		Player requestPlayer();

		boolean shootBullet(Enemy enemy);
	}

	public void setListener(EnemyListener l)
	{
		this.listener = l;
	}

	public Enemy(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		sprite = new Sprite(pX, pY, pTextureRegion, pVertexBufferObjectManager){
			@Override
			protected void onManagedUpdate(float pSecondsElapsed)
			{
				super.onManagedUpdate(pSecondsElapsed);
				Player p = listener.requestPlayer();
	
					makeMove(p, pSecondsElapsed);

			}
		};
	}
	
	public Enemy(float pX, float pY, ITiledTextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		sprite = new AnimatedSprite(pX, pY, pTextureRegion, pVertexBufferObjectManager){
			@Override
			protected void onManagedUpdate(float pSecondsElapsed)
			{
				super.onManagedUpdate(pSecondsElapsed);
				Player p = listener.requestPlayer();
				makeMove(p, pSecondsElapsed);

			}
		};
		animateSprite();
	}

	public void makeMove(Player p, float elapsedTime)
	{

		listener.enemyMoved(this);
	}

	public void takeDamage(int damage)
	{
		health -= damage;
		if (isDead())
		{
			velocityX = 0;
			velocityY = 0;
		}
	}

	public boolean isDead()
	{
		return health <= 0;
	}

	abstract public int getScore();

	public void setHit(boolean b)
	{
		hit = b;

	}

	public boolean isHit()
	{
		return hit;
	}

	public float getCenterX()
	{
		return sprite.getX() + sprite.getWidth() / 2;
	}

	public float getCenterY()
	{
		return sprite.getY() + sprite.getHeight() / 2;
	}



	public void setVelocityX(float f)
	{
		velocityX = f;
	}

	public void setVelocityY(float f)
	{
		velocityY = f;

	}
	
	
	protected float getDistance(Player p)
	{
		return  Math.abs(p.getCenterX() - this.getCenterX())
				+ Math.abs(p.getCenterY() - this.getCenterY());
	}
	
	protected void animateSprite()
	{
		if (sprite instanceof AnimatedSprite)
		{
			((AnimatedSprite)sprite).animate(100);
		}
	}
	@Override
	public Body getBody()
	{
		return body;
	}
	
	
	@Override
	public Sprite getSprite()
	{
		return sprite;
	}

}
