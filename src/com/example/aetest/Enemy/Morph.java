package com.example.aetest.Enemy;

import java.util.Random;

import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.example.aetest.Player.Player;

public class Morph extends Enemy
{

	public Morph(float pX, float pY, ITiledTextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);

	}

	float elapsedTotal = 0;
	private static final float elapsedRequired = 0.3f;

	Random rand = new Random();

	

	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		// setX(getX() + velocityX);
		// setY(getY() + velocityY);
		elapsedTotal += elapsedTime;

		body.setLinearVelocity(velocityX, velocityY);
		super.makeMove(p, elapsedTime);
	}

	@Override
	public int getScore()
	{
		return 5;
	}

}
