package com.example.aetest.Enemy;

import java.util.Random;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.example.aetest.Player.Player;

public class Slimey extends Enemy
{

	float elapsedTotal = 0;
	private static final float elapsedRequired = 1.0f;
	
	Random rand = new Random();

	public Slimey(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		//setX(getX() + velocityX);
		//setY(getY() + velocityY);
		elapsedTotal+= elapsedTime;

		body.setLinearVelocity(velocityX, velocityY);
		super.makeMove(p, elapsedTime);
	}
	
	@Override
	public int getScore()
	{
		return 15;
	}

	public boolean shouldMakeTrail()
	{
		if (elapsedTotal  > elapsedRequired *( 1 /(Math.abs(velocityX) + Math.abs(velocityY)) ) )
		{
			elapsedTotal = 0;
			return true;
		}
		return false;
	}

}
