package com.example.aetest.Enemy;

import java.util.Random;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.example.aetest.Player.Player;

public class Dust extends Enemy
{

	Random rand = new Random();

	public Dust(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
	}

	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		//setX(getX() + velocityX);
		//setY(getY() + velocityY);
		body.setLinearVelocity(velocityX, velocityY);
		super.makeMove(p, elapsedTime);
	}
	
	@Override
	public int getScore()
	{
		return 5;
	}

}
