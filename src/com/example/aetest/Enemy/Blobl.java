package com.example.aetest.Enemy;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.example.aetest.Player.Player;

public class Blobl extends Enemy
{

	float elapsedTotal = 0;
	private static final float elapsedRequired = 0.15f;
	
	public Blobl(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.health = 5;
	}


	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		elapsedTotal+= elapsedTime;
		float maxVelocity = getMaxVelocity(p);
		float currentVelocity = Math.abs(velocityX) + Math.abs(velocityY) ;
		float distance = getDistance(p);
		
		
		if (elapsedTotal > elapsedRequired && currentVelocity < maxVelocity && !isDead()) // If close to player! more speed
		{
			elapsedTotal = 0;
			if (distance < 100)
			{
				if (p.getX() > sprite.getX())
					velocityX+=5;
				else if (p.getX() < sprite.getX())
					velocityX-=5;

				if (p.getY() > sprite.getY())
					velocityY+=5;
				else if (p.getY() < sprite.getY())
					velocityY-=5;
			}
			else
			{
				if (p.getX() > sprite.getX())
					velocityX++;
				else if (p.getX() < sprite.getX())
					velocityX--;
	
				if (p.getY() > sprite.getY())
					velocityY++;
				else if (p.getY() < sprite.getY())
					velocityY--;
			}

		}
		else if ( currentVelocity > maxVelocity) // SLOW DOWN!
		{
			if (velocityX > 0)
			{
				velocityX --;
			}
			else
			{
				velocityX++;
			}
			
			if (velocityY > 0)
			{
				velocityY--;
			}
			else
			{
				velocityY++;
			}
		}
		body.setLinearVelocity(velocityX, velocityY);
		super.makeMove(p, elapsedTime);
	}

	private float getMaxVelocity(Player p)
	{
		float distance = getDistance(p);
		if (distance < 100)
		{
			return 60;
		}
		return 30;
	}


	@Override
	public int getScore()
	{
		return 15;
	}

}
