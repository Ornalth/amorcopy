package com.example.aetest.Enemy;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.example.aetest.Player.Player;

public class Cactus extends Enemy
{

	private static final int MAX_VELOCITY = 30;
	float elapsedTotal = 0;
	private static final float elapsedRequired = 0.15f;
	
	public Cactus(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
		this.health = 5;
	}


	@Override
	public void makeMove(Player p, float elapsedTime)
	{
		elapsedTotal+= elapsedTime;
		if (elapsedTotal > elapsedRequired && Math.abs(velocityX) + Math.abs(velocityY) < MAX_VELOCITY && !isDead())
		{
			elapsedTotal = 0;
			if (p.getX() > sprite.getX())
				velocityX++;
			else if (p.getX() < sprite.getX())
				velocityX--;

			if (p.getY() > sprite.getY())
				velocityY++;
			else if (p.getY() < sprite.getY())
				velocityY--;

		}
		body.setLinearVelocity(velocityX, velocityY);
		super.makeMove(p, elapsedTime);
	}

	@Override
	public int getScore()
	{
		return 10;
	}

}
