package com.example.aetest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.IEntityModifier.IEntityModifierListener;
import org.andengine.entity.particle.BatchedPseudoSpriteParticleSystem;
import org.andengine.entity.particle.SpriteParticleSystem;
import org.andengine.entity.particle.emitter.CircleParticleEmitter;
import org.andengine.entity.particle.emitter.PointParticleEmitter;
import org.andengine.entity.particle.initializer.AlphaParticleInitializer;
import org.andengine.entity.particle.initializer.BlendFunctionParticleInitializer;
import org.andengine.entity.particle.initializer.RotationParticleInitializer;
import org.andengine.entity.particle.initializer.VelocityParticleInitializer;
import org.andengine.entity.particle.modifier.AlphaParticleModifier;
import org.andengine.entity.particle.modifier.ExpireParticleInitializer;
import org.andengine.entity.particle.modifier.ScaleParticleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.TextMenuItem;
import org.andengine.entity.sprite.ButtonSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.adt.io.in.IInputStreamOpener;
import org.andengine.util.color.Color;
import org.andengine.util.debug.Debug;
import org.andengine.util.modifier.IModifier;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.opengl.GLES20;
import android.preference.PreferenceManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.example.aetest.Enemy.Blobl;
import com.example.aetest.Enemy.Cactus;
import com.example.aetest.Enemy.Dust;
import com.example.aetest.Enemy.Enemy;
import com.example.aetest.Enemy.Enemy.EnemyListener;
import com.example.aetest.Enemy.MafiaBlob;
import com.example.aetest.Enemy.Morph;
import com.example.aetest.Enemy.Slimey;
import com.example.aetest.Player.Player;
import com.example.aetest.Player.Player.PlayerListener;
import com.example.aetest.Player.StatusEffect;
import com.example.aetest.Weapon.Blade;
import com.example.aetest.Weapon.Knife;
import com.example.aetest.Weapon.Mace;
import com.example.aetest.Weapon.Spear;
import com.example.aetest.Weapon.Weapon;

//Look at.
//https://github.com/blankwall/BWP

//TODO trail
//http://www.andengine.org/forums/gles2/using-particle-system-or-motion-streak-for-dragged-sprite-t8853.html

public class MainActivity extends BaseGameActivity implements
		IOnSceneTouchListener, PlayerListener, EnemyListener
{
	private static final String PLAYER_BODY_TAG = "player feed";
	private static final String ENEMY_BODY_TAG = "enemy feed";
	private static final String WEAPON_BODY_TAG = "weapon feed";
	private static final String WALL_BODY_TAG = "wall stop feeding";
	private static final String MISC_BODY_TAG = "bullet feeding";

	Random rand = new Random();
	Player player;
	private static int CAMERA_WIDTH = 800;
	private static int CAMERA_HEIGHT = 480;

	public static final short CATEGORYBIT_WALL = 1;
	public static final short CATEGORYBIT_ENEMY = 2;
	public static final short CATEGORYBIT_PLAYER = 4;
	public static final short CATEGORYBIT_WEAPON = 8;
	public static final short CATEGORYBIT_MISC = 16;
	public static final short MASKBITS_WALL = CATEGORYBIT_WALL
			+ CATEGORYBIT_PLAYER;
	public static final short MASKBITS_ENEMY = CATEGORYBIT_WEAPON
			+ CATEGORYBIT_PLAYER + CATEGORYBIT_ENEMY;
	public static final short MASKBITS_PLAYER = CATEGORYBIT_WALL
			+ CATEGORYBIT_ENEMY + CATEGORYBIT_MISC;
	public static final short MASKBITS_MISC = CATEGORYBIT_PLAYER;
	public static final short MASKBITS_WEAPON = CATEGORYBIT_ENEMY;
	private static final int MAX_ENEMIES_FOR_NOW = 8;
	private static final int NUM_ENEMIES = 6;
	private static final float BULLET_VELOCITY = 15;
	private static int CURR_WEAPON_TEST_NUMBER = 2;
	private FixtureDef MISC_FIX = PhysicsFactory.createFixtureDef(0.0f, 0.0f,
			0.0f, false, CATEGORYBIT_MISC, MASKBITS_MISC, (short) 0);

	// private Scene scene;
	BitmapTextureAtlas morphTextureAtlas;
	ITiledTextureRegion morphPicture;
	
	ITextureRegion bombPicture, playerPicture;
	ITextureRegion weapon1Picture, weapon2Picture, weapon3Picture,
			weapon4Picture;
	ITextureRegion atkPicture;
	ITextureRegion dustPicture;
	ITextureRegion cactusPicture;
	ITextureRegion bloblPicture;
	ITextureRegion dropPicture;
	ITextureRegion bulletPicture;
	ITextureRegion mafiaPicture;
	ITextureRegion slimeyPicture;
	ITextureRegion trailPicture;

	private Text scoreView;

	PhysicsWorld physicsWorld;
	Body playerBody;
	Weapon weapons[] = new Weapon[4];

	SceneManager manager;

	private BitmapTextureAtlas mFontTexture;
	public Font font;
	Camera camera;

	private boolean isGameOver = true;
	int score = 0;
	ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	ArrayList<Bullet> bullets = new ArrayList<Bullet>();

	@Override
	public EngineOptions onCreateEngineOptions()
	{
		camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
				new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
	}

	@Override
	public Engine onCreateEngine(EngineOptions pEngineOptions)
	{
		return new LimitedFPSEngine(pEngineOptions, 30);
	}

	@Override
	public void onCreateResources(
			OnCreateResourcesCallback pOnCreateResourcesCallback)
			throws Exception
	{
		manager = new SceneManager();

		mFontTexture = new BitmapTextureAtlas(mEngine.getTextureManager(), 256,
				256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		font = new Font(this.getFontManager(), this.mFontTexture,
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32, true,
				Color.BLACK);

		this.mEngine.getTextureManager().loadTexture(this.mFontTexture);
		this.getFontManager().loadFont(font);
		loadGfx();
		pOnCreateResourcesCallback.onCreateResourcesFinished();

	}

	private void loadGfx()
	{

		/*
		 * BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("pics/");
		 * BuildableBitmapTextureAtlas mBuildableBitmapTextureAtlas = new
		 * BuildableBitmapTextureAtlas(getTextureManager(), 256,256); // width
		 * and height power of 2^x
		 * 
		 * bombPicture = BitmapTextureAtlasTextureRegionFactory
		 * .createFromAsset(mBuildableBitmapTextureAtlas, this, "player.png");
		 * 
		 * mBuildableBitmapTextureAtlas.load();
		 */

		//BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("pics/");
		ITexture playerPicture2, weapon1, atk;
		try
		{

			atk = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/png3.png");
						}
					});

			playerPicture2 = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/player.png");
						}
					});

			weapon1 = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/blade1.png");
						}
					});
			ITexture weapon2 = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/knoife.png");
						}
					});

			ITexture weapon3 = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/stick.png");
						}
					});

			ITexture weapon4 = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/test.png");
						}
					});

			ITexture dust = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/dust.png");
						}
					});
			ITexture cactus = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/cactus.png");
						}
					});
			ITexture enemyblob = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/blobl.png");
						}
					});

			ITexture water = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/drop.png");
						}
					});

			ITexture mafia = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/mafia.png");
						}
					});

			ITexture bullet = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/bullet.png");
						}
					});

			ITexture slime = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/slimey.png");
						}
					});

			ITexture trail = new BitmapTexture(this.getTextureManager(),
					new IInputStreamOpener()
					{
						@Override
						public InputStream open() throws IOException
						{
							return getAssets().open("pics/slimeyTrail.png");
						}
					});

			// bombPicture2.load();
			playerPicture2.load();
			weapon1.load();
			atk.load();
			dust.load();
			water.load();
			cactus.load();
			weapon2.load();
			weapon3.load();
			weapon4.load();
			enemyblob.load();
			bullet.load();
			mafia.load();
			slime.load();
			trail.load();
			slimeyPicture = TextureRegionFactory.extractFromTexture(slime);
			trailPicture = TextureRegionFactory.extractFromTexture(trail);
			mafiaPicture = TextureRegionFactory.extractFromTexture(mafia);
			bulletPicture = TextureRegionFactory.extractFromTexture(bullet);
			bloblPicture = TextureRegionFactory.extractFromTexture(enemyblob);
			cactusPicture = TextureRegionFactory.extractFromTexture(cactus);
			// bombPicture =
			// TextureRegionFactory.extractFromTexture(bombPicture2);
			playerPicture = TextureRegionFactory
					.extractFromTexture(playerPicture2);
			weapon1Picture = TextureRegionFactory.extractFromTexture(weapon1);
			atkPicture = TextureRegionFactory.extractFromTexture(atk);
			dustPicture = TextureRegionFactory.extractFromTexture(dust);
			dropPicture = TextureRegionFactory.extractFromTexture(water);
			weapon2Picture = TextureRegionFactory.extractFromTexture(weapon2);
			weapon3Picture = TextureRegionFactory.extractFromTexture(weapon3);
			weapon4Picture = TextureRegionFactory.extractFromTexture(weapon4);
			
			morphTextureAtlas= new BitmapTextureAtlas(this.getTextureManager(), 256, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
			//4 col, 1 row;
			morphTextureAtlas.load();
			morphPicture = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(morphTextureAtlas, this.getAssets(),"pics/morphTogether.png", 0, 0, 4, 1);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		// 2 - Load bitmap textures into VRAM

	}

	@Override
	public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback)
			throws Exception
	{
		pOnCreateSceneCallback.onCreateSceneFinished(manager.createMainScene());
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent)
	{

		if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN
				&& !isGameOver)
		{

			final float touchX = pSceneTouchEvent.getX();
			final float touchY = pSceneTouchEvent.getY();
			player.setGoalLocation(touchX, touchY);
			return true;

		}
		return false;
	}

	private void createSpriteSpawnTimeHandler()
	{
		TimerHandler spriteTimerHandler;
		float mEffectSpawnDelay = 1f;

		spriteTimerHandler = new TimerHandler(mEffectSpawnDelay, true,
				new ITimerCallback()
				{

					@Override
					public void onTimePassed(TimerHandler pTimerHandler)
					{
						// dropBomb();
						// player.timeTicked();
						addEnemy();

					}
				});

		getEngine().registerUpdateHandler(spriteTimerHandler);
		/*
				getEngine().registerUpdateHandler(
						new TimerHandler(0.05f, true, new ITimerCallback()
						{

							@Override
							public void onTimePassed(TimerHandler pTimerHandler)
							{

								particleEmitter.setCenter(player.getX(), player.getY());

							}
						}));*/
	}

	private void addEnemy()
	{
		if (enemies.size() <= getMaxEnemiesOnScreen())
		{
			createEnemy();
		}
	}

	private int getMaxEnemiesOnScreen()
	{
		return MAX_ENEMIES_FOR_NOW;
	}

	private void createEnemy()
	{
		Enemy enemy = null;
		float height;
		float width;
		float vX;
		float vY;
		boolean b = true;
		while (true)
		{
			switch (rand.nextInt(NUM_ENEMIES))
			{
				case 0:
				{
					enemy = new Dust(0, 0, dustPicture.deepCopy(),
							getVertexBufferObjectManager());

					break;
				}
				case 1:
				{
					enemy = new Cactus(0, 0, cactusPicture.deepCopy(),
							getVertexBufferObjectManager());
					break;

				}
				case 2:
				{
					enemy = new Blobl(0, 0, bloblPicture.deepCopy(),
							getVertexBufferObjectManager());
					break;
				}
				case 3:
				{
					enemy = new MafiaBlob(0, 0, mafiaPicture.deepCopy(),
							getVertexBufferObjectManager());
					break;
				}
				case 4:
				{
					//Max one slimey on the screen to prevent lag!
					int numSlimey = countEnemyType(Slimey.class);
					if (numSlimey > 0)
					{
						break;
					}
					enemy = new Slimey(0, 0, slimeyPicture.deepCopy(),
							getVertexBufferObjectManager());
					break;
				}
				case 5:
				{
					enemy = new Morph(0, 0, morphPicture.deepCopy(), getVertexBufferObjectManager());
					break;
				}
				// /enemy.registerEntityModifier(new LoopEntityModifier(
				// new RotationModifier(5, 0, 360)));
			}
			if (enemy != null)
			{
				break;
			}
			else
			{
				return;
			}
		}

		if (rand.nextInt(2) == 0) // start from top/bot of screen
		{
			// for now only do top
			Sprite spr = enemy.sprite;
			spr.setY(2 - spr.getHeight());
			spr.setX(rand.nextFloat() * CAMERA_WIDTH);
			enemy.setVelocityX(rand.nextFloat() * 5 - 2);
			enemy.setVelocityY(rand.nextFloat() * 10);
		}
		else
		// start at LR // for now nly do L -> R
		{
			Sprite spr = enemy.sprite;
			spr.setY(rand.nextFloat() * CAMERA_HEIGHT);
			spr.setX(2 - spr.getWidth());
			enemy.setVelocityX(rand.nextFloat() * 10);
			enemy.setVelocityY(rand.nextFloat() * 5 - 2);
		}

		FixtureDef WALL_FIX = PhysicsFactory.createFixtureDef(0.0f, 0.0f, 0.0f,
				false, CATEGORYBIT_ENEMY, MASKBITS_ENEMY, (short) 0);

		Body body = PhysicsFactory.createCircleBody(physicsWorld, enemy.sprite,
				BodyType.DynamicBody, WALL_FIX);
		body.setUserData(ENEMY_BODY_TAG);
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(enemy.sprite, body,
				true, true));
		enemy.body = body;
		enemy.setListener(this);
		mEngine.getScene().attachChild(enemy.sprite);
		enemies.add(enemy);
	}
	
	private int countEnemyType(Class class1)
	{
		int count = 0;
		for (int i = 0; i < enemies.size(); i++)
		{
			if (enemies.get(i).getClass().equals(class1))
			{
				count++;
			}
		}
		return count;
	}

	public void removeSprite(final Sprite _sprite, Iterator it)
	{
		runOnUpdateThread(new Runnable()
		{

			@Override
			public void run()
			{

				mEngine.getScene().detachChild(_sprite);
			}
		});
		it.remove();
	}

	@Override
	public void onPopulateScene(Scene pScene,
			OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception
	{
		pOnPopulateSceneCallback.onPopulateSceneFinished();
	}

	public void createExplosion(final float posX, final float posY,
			final SpriteBodyMerge target)
	{

		startEmittingParticles(posX, posY, 5, target);
		return;
		/*
		 * int mNumPart = 9; int mTimePart = 2;
		 * 
		 * PointParticleEmitter particleEmitter = new PointParticleEmitter(posX,
		 * posY); IEntityFactory<Rectangle> recFact = new
		 * IEntityFactory<Rectangle>() {
		 * 
		 * @Override public Rectangle create(float pX, float pY) { Rectangle
		 * rect = new Rectangle(posX, posY, 10, 10,
		 * getVertexBufferObjectManager()); rect.setColor(Color.RED); return
		 * rect; } }; final ParticleSystem<Rectangle> particleSystem = new
		 * ParticleSystem<Rectangle>( recFact, particleEmitter, 500, 500,
		 * mNumPart);
		 * 
		 * particleSystem .addParticleInitializer(new
		 * VelocityParticleInitializer<Rectangle>( -50, 50, -50, 50));
		 * 
		 * particleSystem .addParticleModifier(new
		 * AlphaParticleModifier<Rectangle>(0, 0.6f * mTimePart, 1, 0));
		 * particleSystem .addParticleModifier(new
		 * RotationParticleModifier<Rectangle>(0, mTimePart, 0, 360));
		 * 
		 * target.attachChild(particleSystem);
		 * 
		 * target.registerUpdateHandler(new TimerHandler(mTimePart, new
		 * ITimerCallback() {
		 * 
		 * @Override public void onTimePassed(final TimerHandler pTimerHandler)
		 * { particleSystem.detachSelf(); target.sortChildren();
		 * target.unregisterUpdateHandler(pTimerHandler); } }));
		 */
	}

	public void startEmittingParticles(float centerX, float centerY,
			int radius, final SpriteBodyMerge sb)
	{
		final CircleParticleEmitter particleEmitter = new CircleParticleEmitter(
				centerX, centerY, radius);
		final BatchedPseudoSpriteParticleSystem particleSystem = new BatchedPseudoSpriteParticleSystem(
				particleEmitter, 50, 50, 150, dropPicture,// sprite.getTextureRegion(),
				this.getVertexBufferObjectManager());

		particleSystem.setBlendFunction(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE);

		particleSystem
				.addParticleInitializer(new AlphaParticleInitializer<Entity>(1));

		particleSystem
				.addParticleInitializer(new VelocityParticleInitializer<Entity>(
						10, 150, 10, 150));
		particleSystem
				.addParticleInitializer(new RotationParticleInitializer<Entity>(
						0.0f, 360.0f));
		particleSystem
				.addParticleInitializer(new ExpireParticleInitializer<Entity>(
						0.5f));

		particleSystem.addParticleModifier(new ScaleParticleModifier<Entity>(0,
				0.5f, 0.1f, 0.2f));

		mEngine.getScene().attachChild(particleSystem);
		mEngine.getScene().registerEntityModifier(
				new DelayModifier(1, new IEntityModifierListener()
				{

					@Override
					public void onModifierStarted(IModifier<IEntity> pModifier,
							IEntity pItem)
					{
					}

					@Override
					public void onModifierFinished(
							IModifier<IEntity> pModifier, IEntity pItem)
					{
						runOnUpdateThread(new Runnable()
						{
							@Override
							public void run()
							{
								// TODO destroy physics body
								mEngine.getScene().detachChild(particleSystem);
								sb.getSprite().setVisible(false);
								mEngine.getScene().detachChild(sb.getSprite());
								physicsWorld.destroyBody(sb.getBody());
								enemies.remove(sb.getSprite());
								// physicsWorld.destroyBody(sprite.getBody());
							}
						});
					}
				}));
	}

	private void gameOver()
	{
		Debug.d("game over?");
		isGameOver = true;

		player.stopMovement();
		Text gameOverText = new Text(0, 0, font, "GameOver",
				this.getVertexBufferObjectManager())
		{

			@Override
			public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
					float pTouchAreaLocalX, float pTouchAreaLocalY)
			{
				if (pSceneTouchEvent.isActionUp())
				{
					mEngine.clearUpdateHandlers(); // stop spawning creeps.
					manager.setSceneType(SceneType.MAIN);
				}
				return true;
			}

		};

		gameOverText.setPosition(
				CAMERA_WIDTH / 2 - gameOverText.getWidth() / 2, CAMERA_HEIGHT
						/ 2 - gameOverText.getHeight() / 2);
		Scene scene = mEngine.getScene();
		Scene childScene = new Scene();
		childScene.setBackgroundEnabled(false);
		childScene.attachChild(gameOverText);

		// mEngine.getScene().attachChild(gameOverText);
		//
		scene.clearTouchAreas();

		scene.attachChild(childScene);
		scene.registerTouchArea(gameOverText);

	}

	private void addScore(int sc)
	{
		this.score += sc;
		scoreView.setText("Score: " + score);
	}

	@Override
	public void hasFinishedSwinging(Player player)
	{
		for (Enemy enemy : enemies)
		{
			if (!enemy.isDead())
				enemy.setHit(false);
		}
	}

	/*
	 * public ParticleSystem createParticleSystem(final TextureRegion
	 * waterDropTextureRegion) { //X & Y for the particles to spawn at. final
	 * float particlesXSpawn = 400; final float particlesYSpawn = 300;
	 * 
	 * //Max & min rate are the maximum particles per second and the minimum
	 * particles per second. final float maxRate = 10; final float minRate = 5;
	 * 
	 * //This variable determines the maximum particles in the particle system.
	 * final int maxParticles = 20;
	 * 
	 * //Particle emitter which will set all of the particles at a ertain point
	 * when they are initialized. final PointParticleEmitter
	 * pointParticleEmtitter = new PointParticleEmitter(particlesXSpawn,
	 * particlesYSpawn);
	 * 
	 * //Creating the particle system. final ParticleSystem particleSystem = new
	 * ParticleSystem(pointParticleEmtitter, maxRate, minRate, maxParticles,
	 * waterDropTextureRegion);
	 * 
	 * //And now, lets create the initiallizers and modifiers. //Velocity
	 * initiallizer - will pick a random velocity from -20 to 20 on the x & y
	 * axes. Play around with this value.
	 * particleSystem.addParticleInitializer(new VelocityInitializer(-20, 20,
	 * -20, 20));
	 * 
	 * //Acceleration initializer - gives all the particles the earth gravity
	 * (so they accelerate down). particleSystem.addParticleInitializer(new
	 * GravityInitializer());
	 * 
	 * //And now, adding an alpha modifier, so particles slowly fade out. This
	 * makes a particle go from alpha = 1 to alpha = 0 in 3 seconds, starting
	 * exactly when the particle is spawned.
	 * particleSystem.addParticleModifier(new AlphaModifier(1, 0, 0, 3));
	 * 
	 * //Lastly, expire modifier. Make particles die after 3 seconds - their
	 * alpha reached 0. particleSystem.addParticleModifier(new
	 * ExpireModifier(3));
	 * 
	 * return particleSystem; }
	 */

	private boolean isOffscreen(Sprite sprite)
	{
		if (sprite.getX() > CAMERA_WIDTH
				|| sprite.getX() + sprite.getWidth() < 0)
		{
			return true;
		}

		if (sprite.getY() > CAMERA_HEIGHT
				|| sprite.getY() + sprite.getHeight() < 0)
		{
			return true;
		}
		return false;
	}

	@Override
	public void enemyMoved(final Enemy enemy)
	{
		if (isOffscreen(enemy.sprite))
		{
			runOnUpdateThread(new Runnable()
			{

				@Override
				public void run()
				{
					enemy.sprite.setVisible(false);
					mEngine.getScene().detachChild(enemy.sprite);
					physicsWorld.destroyBody(enemy.getBody());
					enemies.remove(enemy);
				}
			});
		}

		if (enemy instanceof Slimey && ((Slimey) enemy).shouldMakeTrail())
		{
			/*
			Line l = new Line(enemy.getX(), enemy.getY(), enemy.getX() + 1, enemy.getY() + 1, null);
			Line bodyLine = new Line(enemy.getX(), enemy.getY(), enemy.getX() + 1, enemy.getY() + 1, null);
				Body lb;
				Line line = new Line (enemy.getX(), enemy.getY(), enemy.getX() + 1, enemy.getY() + 1, null);
				lb = PhysicsFactory.createLineBody(physicsWorld, bodyLine,
			         MISC_FIX);
				lb.setUserData(MISC_BODY_TAG);
			 line.setLineWidth(5);
				bodyLine.setVisible(false);
			line.setColor(Color.BLACK);
			bodyLine.setColor(255, 0, 0);
			mEngine.getScene().attachChild(bodyLine);
			mEngine.getScene().attachChild(line);*/

			final Sprite sprite = new Sprite(enemy.getCenterX(),
					enemy.getCenterY(), trailPicture.deepCopy(),
					getVertexBufferObjectManager());

			final Body body = PhysicsFactory.createBoxBody(physicsWorld,
					sprite, BodyType.StaticBody, MISC_FIX);
			body.setUserData(MISC_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite,
					body, true, true));

			final Scene scene = mEngine.getScene();
			scene.attachChild(sprite);
			sprite.registerEntityModifier(new AlphaModifier(5f, 1f, 0f,
					new IEntityModifierListener()
					{

						@Override
						public void onModifierStarted(
								IModifier<IEntity> pModifier, IEntity pItem)
						{

						}

						@Override
						public void onModifierFinished(
								IModifier<IEntity> pModifier, IEntity pItem)
						{
							runOnUpdateThread(new Runnable()
							{

								@Override
								public void run()
								{
									sprite.setVisible(false);
									scene.detachChild(sprite);
									physicsWorld.destroyBody(body);
								}
							});
						}
					}));
		}
	}

	@Override
	public Player requestPlayer()
	{
		return player;
	}

	@Override
	public boolean shootBullet(Enemy enemy)
	{
		final Bullet bullet;
		if (enemy instanceof MafiaBlob && rand.nextInt(10) > 2)
		{
			bullet = new Bullet(enemy.getCenterX(), enemy.getCenterY(),
					bulletPicture.deepCopy(), getVertexBufferObjectManager())
			{
				@Override
				protected void onManagedUpdate(final float pSecondsElapsed)
				{
					super.onManagedUpdate(pSecondsElapsed);
					if (isOffscreen(this))
					{
						final Bullet wtf = this;
						runOnUpdateThread(new Runnable()
						{
							@Override
							public void run()
							{
								wtf.setVisible(false);
								mEngine.getScene().detachChild(wtf);
								physicsWorld.destroyBody(wtf.getBody());
								bullets.remove(wtf);
							}
						});

					}

				}
			};

			Body body = PhysicsFactory.createBoxBody(physicsWorld, bullet,
					BodyType.DynamicBody, MISC_FIX);
			body.setUserData(MISC_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(bullet,
					body, true, true));
			bullet.setBody(body);

			mEngine.getScene().attachChild(bullet);
			bullets.add(bullet);
			float changeX = player.getX() - bullet.getX();
			float changeY = player.getY() - bullet.getY();
			float percentX = changeX / (Math.abs(changeX) + Math.abs(changeY));
			float percentY = changeY / (Math.abs(changeX) + Math.abs(changeY));
			body.setLinearVelocity(BULLET_VELOCITY * percentX, BULLET_VELOCITY
					* percentY);
			return true;
		}
		return false;

	}

	public enum SceneType
	{
		MAIN, GAME, OPTIONS;
	}

	private class SceneManager implements IOnMenuItemClickListener
	{
		private SceneType sceneType;

		private Scene gameScene;
		private MenuScene mainScene, optionsScene;

		private static final int MENU_START_ID = 256;
		private static final int MENU_OPTIONS_ID = 512;
		private static final int WEAPON_ID_0 = 0;
		private static final int WEAPON_ID_1 = 1;
		private static final int WEAPON_ID_2 = 2;
		private static final int WEAPON_ID_3 = 3;
		private static final int OPTION_RETURN_MAIN = 4;

		public SceneManager()
		{
		}

		public SceneType getSceneType()
		{
			return sceneType;
		}

		public void setSceneType(SceneType sceneType)
		{
			this.sceneType = sceneType;
			switch (sceneType)
			{
				case GAME:
					mEngine.setScene(gameScene);
					break;
				case MAIN:
					mEngine.setScene(mainScene);
					break;
				case OPTIONS:
					mEngine.setScene(optionsScene);
					break;
				default:
					break;

			}
		}

		public Scene createMainScene()
		{
			mainScene = new MenuScene(camera);
			mainScene.setBackground(new Background(77, 88, 99));
			IMenuItem startButton = new TextMenuItem(MENU_START_ID,
					MainActivity.this.font, "START",
					MainActivity.this.getVertexBufferObjectManager());
			startButton.setPosition(
					camera.getWidth() / 2 - startButton.getWidth() / 2,
					camera.getHeight() / 2 - startButton.getHeight() / 2);

			IMenuItem optionButton = new TextMenuItem(MENU_OPTIONS_ID,
					MainActivity.this.font, "OPTIONS",
					MainActivity.this.getVertexBufferObjectManager());
			optionButton.setPosition(
					camera.getWidth() / 2 - optionButton.getWidth() / 2 + 50,
					camera.getHeight() / 2 - optionButton.getHeight() / 2 + 50);
			mainScene.addMenuItem(startButton);
			mainScene.addMenuItem(optionButton);
			mainScene.setOnMenuItemClickListener(this);

			return mainScene;

		}

		@Override
		public boolean onMenuItemClicked(MenuScene arg0, IMenuItem arg1,
				float arg2, float arg3)
		{
			// arg1.getUserData()
			switch (arg1.getID())
			{
				case MENU_START_ID:
					createGameScene();
					setSceneType(SceneType.GAME);
					return true;
				case MENU_OPTIONS_ID:
					createOptionsScene();
					setSceneType(SceneType.OPTIONS);
					return true;
				case WEAPON_ID_0:
					saveWeapon(0);
					return true;
				case WEAPON_ID_1:
					saveWeapon(1);
					return true;
				case WEAPON_ID_2:
					saveWeapon(2);
					return true;
				case WEAPON_ID_3:
					saveWeapon(3);
					return true;
				case OPTION_RETURN_MAIN:
					setSceneType(SceneType.MAIN);
					return true;
				default:
					break;
			}
			return false;
		}

		// COLLISION DETECTION!
		public Scene createGameScene()
		{
			newGame();
			gameScene = new Scene();
			gameScene.setBackground(new Background(255, 255, 255));
			// gameScene.setBackground(new Background(0, 125, 58));
			physicsWorld = new PhysicsWorld(new Vector2(0, 0), false);

			physicsWorld.setContactListener(new ContactListener()
			{
				@Override
				public void beginContact(final Contact pContact)
				{
					final Body BodyA = pContact.getFixtureA().getBody();
					final Body BodyB = pContact.getFixtureB().getBody();

					if (BodyA.getUserData() != null
							&& BodyB.getUserData() != null)
					{

						if (BodyA.getUserData().equals(PLAYER_BODY_TAG)
								|| BodyB.getUserData().equals(PLAYER_BODY_TAG)
								&& !isGameOver)
						{
							if (BodyA.getUserData().equals(WALL_BODY_TAG)
									|| BodyB.getUserData()
											.equals(WALL_BODY_TAG))
							{
								Debug.d("WALL TOUCHED?");
								player.stopMovement();
							}
							else if (BodyA.getUserData().equals(ENEMY_BODY_TAG)
									|| BodyB.getUserData().equals(
											ENEMY_BODY_TAG))
							{
								Debug.d("GAMEOVER?");
								createExplosion(player.getCenterX(),
										player.getCenterY(), player);
								gameOver();

							}
							else if (BodyA.getUserData().equals(MISC_BODY_TAG)
									|| BodyB.getUserData()
											.equals(MISC_BODY_TAG))
							{
								// HACK TO CHECK FOR BULLET HUE.
								boolean done = false;
								for (Bullet bullet : bullets)
								{
									if (bullet.getBody() == BodyA
											|| bullet.getBody() == BodyB)
									{
										Debug.d("GAMEOVER?");
										createExplosion(player.getCenterX(),
												player.getCenterY(), player);
										gameOver();
										done = true;
										break;
									}
								}

								// Hit slime trail! ( done in presolve now!);
								// if (!done)
								// {
								// player.addStatusEffect(new StatusEffect(
								// StatusEffect.Status.SLIME, 2));
								// }

							}

						}
					}

				}

				@Override
				public void preSolve(Contact pContact, Manifold oldManifold)
				{
					// For weapon to not push enemies!
					final Body BodyA = pContact.getFixtureA().getBody();
					final Body BodyB = pContact.getFixtureB().getBody();

					if (BodyA.getUserData() != null
							&& BodyB.getUserData() != null)
					{

						if (BodyA.getUserData().equals(PLAYER_BODY_TAG)
								|| BodyB.getUserData().equals(PLAYER_BODY_TAG))
						{
							if (isGameOver)
							{
								pContact.setEnabled(false);
							}
							else if (BodyA.getUserData().equals(MISC_BODY_TAG)
									|| BodyB.getUserData()
											.equals(MISC_BODY_TAG))
							{
								boolean isBullet = false;
								for (Bullet bullet : bullets)
								{
									if (bullet.getBody() == BodyA
											|| bullet.getBody() == BodyB)
									{
										isBullet = true;
										break;
									}
								}

								// Hit slime trail!
								if (!isBullet)
								{
									pContact.setEnabled(false);
									player.addStatusEffect(new StatusEffect(
											StatusEffect.Status.SLIME, 5));
								}

							}

						}
						else if (BodyA.getUserData().equals(WEAPON_BODY_TAG)
								|| BodyB.getUserData().equals(WEAPON_BODY_TAG))
						{
							if (BodyA.getUserData().equals(ENEMY_BODY_TAG)
									|| BodyB.getUserData().equals(
											ENEMY_BODY_TAG))
							{
								pContact.setEnabled(false);
								Debug.d("GDAMAGE?");
								for (Enemy enemy : enemies)
								{
									if (enemy.body == BodyA
											|| enemy.body == BodyB)
									{
										// TODO use weapon form this hit. and
										// not
										// actual wtf.
										if (!enemy.isHit())
										{
											enemy.takeDamage(player.weapon
													.getDamage());
											enemy.setHit(true);
											if (enemy.isDead())
											{
												addScore(enemy.getScore());

												createExplosion(
														enemy.getCenterX(),
														enemy.getCenterY(),
														enemy);
												// enemy.setVisible(false);
												// gameScene.detachChild(enemy);

											}
										}
										break;

									}

								}
							}

						}
					}
				}

				@Override
				public void postSolve(Contact contact, ContactImpulse impulse)
				{
				}

				@Override
				public void endContact(final Contact pContact)
				{
				}
			});

			final Rectangle ground = new Rectangle(0, CAMERA_HEIGHT - 2,
					CAMERA_WIDTH, 2, getVertexBufferObjectManager());
			final Rectangle roof = new Rectangle(0, 0, CAMERA_WIDTH, 2,
					getVertexBufferObjectManager());
			final Rectangle left = new Rectangle(0, 0, 2, CAMERA_HEIGHT,
					getVertexBufferObjectManager());
			final Rectangle right = new Rectangle(CAMERA_WIDTH - 2, 0, 2,
					CAMERA_HEIGHT, getVertexBufferObjectManager());

			left.setColor(Color.BLUE);
			right.setColor(Color.BLUE);
			roof.setColor(Color.BLUE);
			ground.setColor(Color.BLUE);

			FixtureDef WALL_FIXTURE_DEF = PhysicsFactory.createFixtureDef(1.0f,
					0.0f, 0.0f, false, CATEGORYBIT_WALL, MASKBITS_WALL,
					(short) 0);
			Body b = PhysicsFactory.createBoxBody(physicsWorld, ground,
					BodyType.StaticBody, WALL_FIXTURE_DEF);
			b.setUserData(WALL_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(ground,
					b, true, true));
			b = PhysicsFactory.createBoxBody(physicsWorld, roof,
					BodyType.StaticBody, WALL_FIXTURE_DEF);
			b.setUserData(WALL_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(roof, b,
					true, true));
			b = PhysicsFactory.createBoxBody(physicsWorld, left,
					BodyType.StaticBody, WALL_FIXTURE_DEF);
			b.setUserData(WALL_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(left, b,
					true, true));
			b = PhysicsFactory.createBoxBody(physicsWorld, right,
					BodyType.StaticBody, WALL_FIXTURE_DEF);
			b.setUserData(WALL_BODY_TAG);

			physicsWorld.registerPhysicsConnector(new PhysicsConnector(right,
					b, true, true));

			this.gameScene.attachChild(ground);
			this.gameScene.attachChild(roof);
			this.gameScene.attachChild(left);
			this.gameScene.attachChild(right);

			scoreView = new Text(0,0, font,
					"And right aligned!\nLorem ipsum dolor sit amat...",
					getVertexBufferObjectManager());
			scoreView.setHorizontalAlign(HorizontalAlign.RIGHT);
			scoreView.setX(CAMERA_WIDTH - 250);
			scoreView.setY(CAMERA_HEIGHT - scoreView.getHeight());
			
			gameScene.attachChild(scoreView);

			gameScene.registerUpdateHandler(physicsWorld);

			player = new Player(247, 346, playerPicture.deepCopy(),
					getVertexBufferObjectManager())
			{
				@Override
				protected void onManagedUpdate(float pSecondsElapsed)
				{
					super.onManagedUpdate(pSecondsElapsed);
					player.timeTicked(pSecondsElapsed);
				}
			};
			player.setListener(MainActivity.this);

			ButtonSprite atkButton = new ButtonSprite(0, 0,
					atkPicture.deepCopy(), getVertexBufferObjectManager())
			{
				@Override
				public boolean onAreaTouched(TouchEvent pTouchEvent,
						float pTouchAreaLocalX, float pTouchAreaLocalY)
				{
					if (pTouchEvent.isActionDown())
					{
						player.beginSwing();
					}
					return true;
				}
			};
			atkButton.setAlpha(0.4f);
			atkButton.setX(0);
			atkButton.setY(CAMERA_HEIGHT - atkButton.getHeight());
			weapons[0] = new Blade(0, 0, weapon1Picture.deepCopy(),
					getVertexBufferObjectManager());
			weapons[1] = new Knife(0, 0, weapon2Picture.deepCopy(),
					getVertexBufferObjectManager());
			weapons[2] = new Spear(0, 0, weapon3Picture.deepCopy(),
					getVertexBufferObjectManager());
			weapons[3] = new Mace(0, 0, weapon4Picture.deepCopy(),
					getVertexBufferObjectManager());

			CURR_WEAPON_TEST_NUMBER = loadWeapon();
			Weapon weapon = weapons[CURR_WEAPON_TEST_NUMBER];
			player.setWeapon(weapon);

			final FixtureDef PLAYER_FIX = PhysicsFactory.createFixtureDef(50f,
					0.05f, 0f, false, CATEGORYBIT_PLAYER, MASKBITS_PLAYER,
					(short) 0);

			playerBody = PhysicsFactory.createCircleBody(physicsWorld, player,
					BodyType.DynamicBody, PLAYER_FIX);
			playerBody.setUserData(PLAYER_BODY_TAG);
			physicsWorld.registerPhysicsConnector(new PhysicsConnector(player,
					playerBody, true, true));/*		{
												@Override
												public void onUpdate(final float pSecondsElapsed)
												{
												super.onUpdate(pSecondsElapsed);
												final Vector2 movingBodyWorldCenter = playerBody
												.getWorldCenter();
												connectionLine.setPosition(
												connectionLine.getX1(),
												connectionLine.getY1(),
												movingBodyWorldCenter.x
												* PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT,
												movingBodyWorldCenter.y
												* PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT);
												}
												});*/
			player.setBody(playerBody);
			//

			final FixtureDef weaponFIX = PhysicsFactory.createFixtureDef(20f,
					0f, 0.0f, false, CATEGORYBIT_WEAPON, MASKBITS_WEAPON,
					(short) 0);

			Body weaponBody = PhysicsFactory.createBoxBody(physicsWorld,
					weapon, BodyType.KinematicBody, weaponFIX);

			physicsWorld.registerPhysicsConnector(new PhysicsConnector(weapon,
					weaponBody, true, true));
			weapon.body = weaponBody;
			weaponBody.setUserData(WEAPON_BODY_TAG);
			weapon.setVisible(false);
			weapon.body.setActive(false);

			// gameScene.registerTouchArea(player);
			gameScene.attachChild(player);
			gameScene.attachChild(weapon);
			gameScene.attachChild(atkButton);
			gameScene.registerTouchArea(atkButton);
			gameScene.setTouchAreaBindingOnActionDownEnabled(true);
			gameScene.setOnSceneTouchListener(MainActivity.this);

			createSpriteSpawnTimeHandler();
			return gameScene;
		}

		public Scene createOptionsScene()
		{
			optionsScene = new MenuScene(camera);
			optionsScene.setBackground(new Background(77, 88, 99));
			IMenuItem startButton = new TextMenuItem(WEAPON_ID_0,
					MainActivity.this.font, "blade",
					MainActivity.this.getVertexBufferObjectManager());
			startButton.setPosition(
					camera.getWidth() / 2 - startButton.getWidth() / 2,
					camera.getHeight() / 2 - startButton.getHeight() / 2);

			IMenuItem optionButton = new TextMenuItem(WEAPON_ID_1,
					MainActivity.this.font, "knife",
					MainActivity.this.getVertexBufferObjectManager());
			optionButton.setPosition(
					camera.getWidth() / 2 - optionButton.getWidth() / 2 + 50,
					camera.getHeight() / 2 - optionButton.getHeight() / 2 + 50);
			IMenuItem bButton = new TextMenuItem(WEAPON_ID_2,
					MainActivity.this.font, "spear",
					MainActivity.this.getVertexBufferObjectManager());
			bButton.setPosition(camera.getWidth() / 2 - bButton.getWidth() / 2
					+ 100, camera.getHeight() / 2 - bButton.getHeight() / 2
					+ 100);

			IMenuItem cButton = new TextMenuItem(WEAPON_ID_3,
					MainActivity.this.font, "mace",
					MainActivity.this.getVertexBufferObjectManager());

			cButton.setPosition(camera.getWidth() / 2 - cButton.getWidth() / 2
					+ 150, camera.getHeight() / 2 - cButton.getHeight() / 2
					+ 150);

			IMenuItem mainButton = new TextMenuItem(OPTION_RETURN_MAIN,
					MainActivity.this.font, "return to main",
					MainActivity.this.getVertexBufferObjectManager());

			mainButton.setPosition(
					camera.getWidth() / 2 - mainButton.getWidth() / 2 + 150,
					camera.getHeight() / 2 - mainButton.getHeight() / 2 + 200);
			optionsScene.addMenuItem(startButton);
			optionsScene.addMenuItem(optionButton);
			optionsScene.addMenuItem(bButton);
			optionsScene.addMenuItem(cButton);
			optionsScene.addMenuItem(mainButton);
			optionsScene.setOnMenuItemClickListener(this);
			return optionsScene;

		}

	}

	private static final String WEAPON_NUMBER = "Blargh";

	public boolean saveWeapon(int weapon)
	{
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		SharedPreferences.Editor prefsEditor = sharedPrefs.edit();
		prefsEditor.putInt(WEAPON_NUMBER, weapon);
		return prefsEditor.commit();
	}

	public int loadWeapon()
	{
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		return sharedPrefs.getInt(WEAPON_NUMBER, 0);
	}

	public void newGame()
	{
		score = 0;
		enemies = new ArrayList<Enemy>();
		bullets = new ArrayList<Bullet>();
		isGameOver = false;
	}

	private PointParticleEmitter particleEmitter;
	private SpriteParticleSystem particleSystem;

	private SpriteParticleSystem initTrail()
	{
		PointParticleEmitter particleEmitter = new PointParticleEmitter(0, 0);
		SpriteParticleSystem particleSystem = new SpriteParticleSystem(
				particleEmitter, 50, 15, 50, trailPicture.deepCopy(),
				getVertexBufferObjectManager());

		particleSystem
				.addParticleInitializer(new AlphaParticleInitializer<Sprite>(0));
		particleSystem
				.addParticleInitializer(new BlendFunctionParticleInitializer<Sprite>(
						GLES20.GL_SRC_ALPHA, GLES20.GL_ONE));
		particleSystem
				.addParticleInitializer(new VelocityParticleInitializer<Sprite>(
						0));
		particleSystem
				.addParticleInitializer(new ExpireParticleInitializer<Sprite>(2));
		particleSystem.addParticleModifier(new ScaleParticleModifier<Sprite>(0,
				2, 1, .5f));
		particleSystem.addParticleModifier(new AlphaParticleModifier<Sprite>(0,
				2, 1, 0));
		return particleSystem;
	}

}
