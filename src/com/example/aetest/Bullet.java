package com.example.aetest;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.physics.box2d.Body;

public class Bullet extends Sprite implements SpriteBodyMerge
{
	private Body body;
	
	public Bullet(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
	}

	@Override
	public Body getBody()
	{
		return body;
	}
	
	@Override
	public Sprite getSprite()
	{
		return this;
	}

	
	public void setBody(Body body)
	{
		this.body = body;
	}

}
