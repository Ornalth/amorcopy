package com.example.aetest.Player;

import java.util.ArrayList;
import java.util.Iterator;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import com.badlogic.gdx.physics.box2d.Body;
import com.example.aetest.SpriteBodyMerge;
import com.example.aetest.Player.StatusEffect.Status;
import com.example.aetest.Weapon.Weapon;

public class Player extends Sprite implements SpriteBodyMerge
{
	PlayerListener listener;
	float goalX = -256;
	float goalY = -256;
	float initialDeg;
	PlayerState state = PlayerState.WAIT;
	public Weapon weapon;
	Body playerBody;

	float baseVelocity = 10;
	float angleSwung = 0;
	float baseTurnRate = 10;
	boolean needsTurning = false;
	int delayCounter = 0;
	ArrayList<StatusEffect> statuses = new ArrayList<StatusEffect>();

	public void setBody(Body body)
	{
		playerBody = body;
	}

	public interface PlayerListener
	{
		void hasFinishedSwinging(Player player);
	}

	public void setListener(PlayerListener l)
	{
		this.listener = l;
	}

	public enum PlayerState
	{
		MOVING, ATTACKING, WAIT;
	}

	public Player(float pX, float pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager pVertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
	}

	public void setGoalLocation(float x, float y)
	{
		goalX = x;
		goalY = y;
		needsTurning = true;
		stopMoving();
		if (state == PlayerState.WAIT)
		{
			state = PlayerState.MOVING;
		}
	}

	private void stopMoving()
	{
		playerBody.setLinearVelocity(0, 0);
		playerBody.setAngularVelocity(0);
	}

	public void beginSwing()
	{
		stopMoving();
		weapon.body.setActive(true);
		weapon.setVisible(true);
		if (state != PlayerState.ATTACKING)
		{
			initialDeg = playerBody.getAngle();

			// Debug.d("CHANGE X " + Math.cos(initialDeg) * 32 + " CJHANGE Y " +
			// Math.sin(initialDeg) * 32);
			centerWeaponSwing(initialDeg);

			// Debug.d("CHANGE X " + (Math.cos(initialDeg)) + " CJHANGE Y " +
			// Math.sin(initialDeg));

			// initialDeg = weapon.getRotation();
			state = PlayerState.ATTACKING;
			angleSwung = 0;
			delayCounter = 0;
		}
	}

	// 256 == 5
	// 128 == 3
	// 64 == 2
	private void centerWeaponSwing(double rad)
	{

		float radians = (float) rad;
		float offsetX = getWidth() / 2 / 32 + (weapon.getWidth() / 2 / 32); // 32
																			// +
																			// 64
		float offsetY = weapon.getWidth() / 64 + 1;// (getHeight()/32 +
													// weapon.getHeight()/ 32) -
													// (weapon.getWidth()/getWidth())
													// + 1; //
		Debug.d("OFFSETX " + offsetX + " OFFSETY" + offsetY);
		weapon.body.setTransform(playerBody.getWorldCenter().x
				- (float) (offsetX * Math.cos(radians))// - weapon.getWidth() /
														// 32
		, playerBody.getWorldCenter().y
				- (float) (offsetY * (Math.sin(radians))) // -
															// weapon.getHeight()/2/
															// 32

		, radians);
	}

	private void continueSwing()
	{
		angleSwung += (weapon.getSwingSpeed());
		// the other way

		if (angleSwung > weapon.getMaxAngle())
		{
			delayCounter++;

			if (delayCounter >= weapon.getDelay())
			{
				state = PlayerState.MOVING;
				weapon.body.setActive(false);
				weapon.setVisible(false);
				listener.hasFinishedSwinging(this);
			}

		}
		else
		{
			centerWeaponSwing(initialDeg - (angleSwung / 180 * Math.PI));
		}
	}

	public void timeTicked(float elapsedTime)
	{
		Iterator<StatusEffect> it = statuses.iterator();
		while (it.hasNext())
		{
			StatusEffect status = it.next();
			if (status.decay(elapsedTime))
			{
				it.remove();
			}
		}
		switch (state)
		{
			case ATTACKING:
				continueSwing();
				break;
			case MOVING:
				continueMove();
				break;
			case WAIT:
				break;
			default:
				break;

		}
	}

	static int counter = 0;

	private void continueMove()
	{
		if (goalX == -256 || goalY == -256)
		{
			state = PlayerState.WAIT;
			return;
		}

		// state = PlayerState.WAIT;

		float changeX = goalX - getCenterX();
		float changeY = goalY - getCenterY();
		float totalChange = Math.abs(changeX) + Math.abs(changeY);
		if (totalChange < 3)
		{
			state = PlayerState.WAIT;
			playerBody.setLinearVelocity(0, 0);
			// playerBody.setLinearVelocity(0,0);
			playerBody.setAngularVelocity(0);

		}
		else
		{
			if (rotateSelf())
			{
				float percentX = changeX / totalChange;
				float percentY = changeY / totalChange;
				float velo = getVelocity();
				if (totalChange < velo)
				{
					playerBody.setLinearVelocity(changeX, changeY);

				}
				else
				{
					playerBody.setLinearVelocity(percentX * velo,
							percentY * velo);

				}
			}

		}
	}

	private boolean rotateSelf()
	{
		if (!needsTurning)
		{
			playerBody.setAngularVelocity(0);
			return true;
		}
		double PI = 3.14159265;
		double angle = Math.atan((goalY - getCenterY())
				/ (goalX - getCenterX()))
				* 180 / PI;
		// Debug.d("Angle is " + angle + " goalY " + (goalY - getCenterY()) +
		// " goalX " + (goalX - getCenterX()));

		float debugGoalX = goalX - getCenterX();
		float debugGoalY = goalY - getCenterY();
		// Debug.d("Actual Angle is " + ((counter%4) * 90) );

		// Skew the actual angle. because sprite is odd....
		if (goalX - getCenterX() > 0)
		{
			angle = angle - 90;
			// setRotation((float)angle);
		}
		else
		{
			angle = angle - 270;
			// setRotation((float)angle);
		}

		float currentAngle = getRotation();

		if (Math.abs(angle - currentAngle) % 360 < getTurnRate())
		{
			playerBody.setAngularVelocity((float) (angle - currentAngle));

			// setRotation((float) angle);
			// weapon.setRotation((float) angle);
			needsTurning = false;
			return true;
		}
		else
		{
			float goalAngle = (float) ((currentAngle - angle) % 360);

			if (goalAngle > 0)
			{
				playerBody.setAngularVelocity(-getTurnRate());
			}
			else
			{
				playerBody.setAngularVelocity(getTurnRate());
			}
			return false;
		}
		// counter++;

		// rotate blade.
		// angle += 90;
		// Debug.d("New Angle is " + angle);
		// double sinY = Math.sin((angle + 90) / 180 * PI);
		// double cosX = Math.cos((angle + 90) / 180 * PI);
		// double changeY = sinY * (weapon.getHeight()/2);
		// double changeX = cosX *( weapon.getWidth()) ;
		// weapon.setX((float) (getX() - changeX ));
		// weapon.setY((float) (getCenterY() - changeY ));

	}

	public float getCenterX()
	{
		return getX() + this.getWidth() / 2;
	}

	public float getCenterY()
	{
		return getY() + this.getHeight() / 2;
	}

	public void setWeapon(Weapon s)
	{
		weapon = s;
		float height = weapon.getHeight();
		float width = weapon.getWidth();
		weapon.setY(getCenterY() - height / 2);
		weapon.setX(getX() - width);

	}

	public PlayerState getState()
	{
		return state;
	}

	public Weapon getWeapon()
	{
		return weapon;
	}

	public void stopMovement()
	{
		needsTurning = false;
		if (state == PlayerState.MOVING)
			state = PlayerState.WAIT;

		goalX = getCenterX();
		goalY = getCenterY();
		stopMoving();
	}

	private float getVelocity()
	{
		float velo = baseVelocity + weapon.getPlayerVelocityChange();
		if (getStatus(StatusEffect.Status.SLIME) != null)
		{
			velo -= 8;
		}
		return velo;
	}

	private StatusEffect getStatus(Status status)
	{
		for (StatusEffect effect : statuses)
		{
			if (status == effect.status)
			{
				return effect;
			}
		}
		return null;
	}

	private float getTurnRate()
	{
		return baseTurnRate + weapon.getPlayerTurnRateChange();
	}

	public void addStatusEffect(StatusEffect statusEffect)
	{
		StatusEffect s = getStatus(statusEffect.status);
		if (s == null)
		{
			statuses.add(statusEffect);
		}
		else
		{
			s.duration = max(statusEffect.duration, s.duration);
		}

	}

	/*
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
	float pTouchAreaLocalX, float pTouchAreaLocalY)
	{

	player.setPosition(pSceneTouchEvent.getX() - this.getWidth() / 2,
		pSceneTouchEvent.getY() - this.getHeight() / 2);
	return true;
	}
	*/
	

	private float max(float duration, float duration2)
	{
		if (duration > duration2)
			return duration;
		return duration2;
	}

	@Override
	public Sprite getSprite()
	{
		return this;
	}


	@Override
	public Body getBody()
	{
		return playerBody;
	}
}
