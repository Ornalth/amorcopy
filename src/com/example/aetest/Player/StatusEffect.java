package com.example.aetest.Player;


public class StatusEffect
{
	//TODO counters or stacks.
	float duration = 0;
	Status status = null;
	public StatusEffect(Status status, float duration)
	{
		this.status = status;
		this.duration = duration;
	}

	public enum Status
	{
		SLIME;
	}

	public boolean decay(float elapsedTime)
	{
		duration -= elapsedTime;
		return duration <= 0;
	}

}
