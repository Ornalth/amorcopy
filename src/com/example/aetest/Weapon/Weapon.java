package com.example.aetest.Weapon;

import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.physics.box2d.Body;
import com.example.aetest.SpriteBodyMerge;

public abstract class Weapon extends Sprite implements SpriteBodyMerge
{
	//TODO maybe change pseed/turn rate of player as well!
	public Body body;
	public Weapon(int pX, int pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager vertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, vertexBufferObjectManager);
	}
	
	abstract public int getDamage();
	abstract public int getDelay();
	abstract public int getMaxAngle();
	abstract public int getSwingSpeed();

	abstract public float getPlayerVelocityChange();
	abstract public float getPlayerTurnRateChange();
	@Override
	public Body getBody()
	{
		return body;
	}
	
	@Override
	public Sprite getSprite()
	{
		return this;
	}
	
}
