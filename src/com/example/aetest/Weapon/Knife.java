package com.example.aetest.Weapon;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Knife extends Weapon
{

	public Knife(int pX, int pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager vertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, vertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getDamage()
	{
		return 15;
	}

	@Override
	public int getDelay()
	{
		return 1;
	}

	@Override
	public int getMaxAngle()
	{
		return 180;
	}

	@Override
	public int getSwingSpeed()
	{
		return 30;
	}

	@Override
	public float getPlayerVelocityChange()
	{
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public float getPlayerTurnRateChange()
	{
		// TODO Auto-generated method stub
		return 5;
	}

}
