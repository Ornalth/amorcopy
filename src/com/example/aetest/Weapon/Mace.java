package com.example.aetest.Weapon;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Mace extends Weapon
{

	public Mace(int pX, int pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager vertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, vertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getDamage()
	{
		return 15;
	}

	@Override
	public int getDelay()
	{
		return 15;
	}

	@Override
	public int getMaxAngle()
	{
		return 60;
	}

	@Override
	public int getSwingSpeed()
	{
		return 20;
	}

	@Override
	public float getPlayerVelocityChange()
	{
		// TODO Auto-generated method stub
		return -2;
	}

	@Override
	public float getPlayerTurnRateChange()
	{
		// TODO Auto-generated method stub
		return -3;
	}

}
