package com.example.aetest.Weapon;

import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

public class Spear extends Weapon
{

	public Spear(int pX, int pY, ITextureRegion pTextureRegion,
			VertexBufferObjectManager vertexBufferObjectManager)
	{
		super(pX, pY, pTextureRegion, vertexBufferObjectManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int getDamage()
	{
		return 10;
	}

	@Override
	public int getDelay()
	{
		return 10;
	}

	@Override
	public int getMaxAngle()
	{
		return 30;
	}

	@Override
	public int getSwingSpeed()
	{
		return 10;
	}

	@Override
	public float getPlayerVelocityChange()
	{
		// TODO Auto-generated method stub
		return -5;
	}

	@Override
	public float getPlayerTurnRateChange()
	{
		// TODO Auto-generated method stub
		return -2;
	}

}
